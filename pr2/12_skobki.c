#include <stdio.h>

int main() {
    int n,i=0,skobsumm=0;
    char temp;
    (void) scanf("%d",&n);
    while (i<n)
    {
        (void) scanf("%c",&temp);
        if (temp=='('||temp==')')
        {
            if(i==0&&temp==')'){
                skobsumm++;
                break;
            }
            else if (temp == '('){
                skobsumm++;
                i++;
            }
            else if (temp == ')'){
                skobsumm--;
                i++;
            }
        }
    }
    if (skobsumm==0){
        printf("\n%d",1);
    }
    else {
        printf("\n%d",0);
    }
    return 0;
}