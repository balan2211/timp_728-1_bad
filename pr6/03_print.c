﻿
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Структура для хранения узла дерева.
// Необходимо хранить ссылки на потомков, предка и некоторое значение
typedef struct node {
    int value;
    struct node* left;
    struct node* righ;
} node;


// Структура для хранения дерева.
// Хранит ссылку на корень дерева и количество элементов в дереве
typedef struct tree {
    struct node* root;
} tree;


// Инициализация дерева
void init(tree* t) {
    t->root = NULL;
};


int insertnode(node* n, int value) {
    if (n != NULL) {
        if (value == n->value) {
            return 1;
        }
        if (n->value < value) {
            if (n->righ == NULL) {
                n->righ = malloc(sizeof(node));
                n->righ->value = value;
                n->righ->righ = NULL;
                n->righ->left = NULL;
                return 0;
            }
            return insertnode(n->righ, value);
        }
        else {
            if (n->left == NULL) {
                n->left = malloc(sizeof(node));
                n->left->value = value;
                n->left->left = NULL;
                n->left->righ = NULL;
                return 0;
            }
            return insertnode(n->left, value);
        }
    }
    else {
        n = malloc(sizeof(node));
        n->value = value;
        n->left = NULL;
        n->righ = NULL;
        return 0;
    }
};

// Вставка значения в дерево:
// 0 - вставка выполнена успешно
// 1 - элемент существует
// 2 - не удалось выделить память для нового элемента
int insert(tree* t, int value) {
    if (t->root == NULL) {
        t->root = malloc(sizeof(node));
        t->root->value = value;
        t->root->left = NULL;
        t->root->righ = NULL;
        return 0;
    }
    else {
        return insertnode(t->root, value);
    }

};


void postOrder(node * n) {
    if (n != NULL) {
        postOrder(n->left);
        postOrder(n->righ);
        printf("%d ",n->value);
    }
}



int main() {
    tree tree;
    node* node1, * node2;
    init(&tree);
    int a;

    for (int i = 0; i < 7; i++) {
        (void)scanf("%d", &a);
        insert(&tree, a);
    }
    postOrder(tree.root);
    
};
