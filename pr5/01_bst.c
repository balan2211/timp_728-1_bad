#include <stdio.h>
#include <stdlib.h>

// Структура для хранения узла дерева.
// Необходимо хранить ссылки на потомков, предка и некоторое значение
typedef struct node {
	int value;
	struct node* left;
	struct node* righ;
} node;


// Структура для хранения дерева.
// Хранит ссылку на корень дерева и количество элементов в дереве
typedef struct tree {
	struct node* root;
} tree;


// Инициализация дерева
void init(tree* t) {
	t->root = NULL;
};

void clean_node(node* n)
{
	if (n != NULL) {
		clean_node(n->left);
		clean_node(n->righ);
		free(n);
	}
}

int how_many_items(node* n)
{
	if (n == NULL) {
		return 0;
	}
		return how_many_items(n->righ) + how_many_items(n->left) + 1;
}

// Удалить все элементы из дерева
void clean(tree* t) {
	clean_node(t->root);
	t->root = NULL;
};

node* find_node(node* n, int value) {
	if (n != NULL) {
		if (n->value == value) {
			return n;
		}
		if (find_node(n->left, value)) {
			return find_node(n->left, value);
		}
		if (find_node(n->righ, value)) {
			return find_node(n->righ, value);
		}
	}
	return NULL;
};

// Поиск элемента по значению. Вернуть NULL если элемент не найден
node* find(tree* t, int value) {
	return find_node(t->root, value);
};

int insertnode(node* n, int value) {
	if (n != NULL) {
		if (value == n->value) {
			return 1;
		}
		if (n->value < value) {
			if (n->righ == NULL) {
				n->righ = malloc(sizeof(node));
				n->righ->value = value;
				n->righ->righ = NULL;
				n->righ->left = NULL;
				return 0;
			}
			return insertnode(n->righ, value);
		}
		else {
			if (n->left == NULL) {
				n->left = malloc(sizeof(node));
				n->left->value = value;
				n->left->left = NULL;
				n->left->righ = NULL;
				return 0;
			}
			return insertnode(n->left, value);
		}
	}
	else {
		n = malloc(sizeof(node));
		n->value = value;
		n->left = NULL;
		n->righ = NULL;
		return 0;
	}
};

// Вставка значения в дерево:
// 0 - вставка выполнена успешно
// 1 - элемент существует
// 2 - не удалось выделить память для нового элемента
int insert(tree* t, int value) {
	if (t->root == NULL) {
		t->root = malloc(sizeof(node));
		t->root->value = value;
		t->root->left = NULL;
		t->root->righ = NULL;
		return 0;
	}
	else {
		return insertnode(t->root, value);
	}

};

node* find_parent(node* n, node* parent, int value) {
	if (n != NULL) {
		if (n->value == value) {
			return parent;
		}
		if (find_parent(n->left, n, value)) {
			return find_parent(n->left, n, value);
		}
		if (find_parent(n->righ, n, value)) {
			return find_parent(n->righ, n, value);
		}
	}
	return NULL;
};

node* remove_this_node(node* n) {
	if (n->left == NULL) {
		if (n->righ == NULL) {//если нет потомков
			free(n);
			return NULL;
		}
		node* temp = n->righ;//если один 
		free(n);
		return temp;
	}
	else {
		if (n->righ == NULL) {//если один
			node* temp = n->left;
			free(n);
			return temp;
		}
		else {//если 2 потомка
			node* temp = n->righ,*node;
			if (temp->left == NULL) {
				temp->left = n->left;
				free(n);
				return temp;
			}
			else {
				while (temp->left->left != NULL) {
					temp = temp->left;
				}
				node = temp->left;
				temp->left = temp->left->righ;
				node->left = n->left;
				node->righ = n->righ;
				free(n);
				return node;
			}

		}
	}
}

// Удалить элемент из дерева:
// 0 - удаление прошло успешно
// 1 - нет элемента с указанным значением
int remove_node(tree* t, int value) {
	node* parent;
	parent = find_parent(t->root, NULL, value);
	if (parent == NULL && t->root->value != value) {
		return 1;
	}
	if (t->root->value == value) {
		t->root = remove_this_node(t->root);
		return 0;
	}
	if (parent->left != NULL && parent->left->value == value) {
		parent->left = remove_this_node(parent->left);
		return 0;
	}
	else {
		parent->righ = remove_this_node(parent->righ);
		return 0;
	}
}


// Удалить минимальный элемент из поддерева, корнем которого является n
// Вернуть значение удаленного элемента
int remove_min(node* n, tree* t) {
	if (n->left == NULL) {
		node* par;
		par = find_parent(t->root, NULL, n->value);
		par->left = n->righ;
	}
	else {
		int val;
		node* temp = n, * fre;
		while (temp->left->left == NULL) {
			temp = temp->left;
		}
		fre = temp->left;
		val = temp->value;
		temp->left = temp->left->righ;
		free(fre);
		return val;
	}
};

// Выполнить правое вращение поддерева, корнем которого является n:
// 0 - успешно выполненная операция
// 1 - вращение невозможно 
int rotate_right(node* n, tree* t) {
	if (n->left == NULL) {
		return 1;
	}
	else {
		node* parent, * temp;
		temp = n->left;
		n->left = n->left->righ;
		temp->righ = n;
		parent = find_parent(t->root, NULL, n->value);
		if (parent == NULL) {
			t->root = temp;
			return 0;
		}
		else {
			if (parent->left->value == n->value) {
				parent->left = temp;
				return 0;
			}
			else {
				parent->righ = temp;
				return 0;
			}

		}
	}
}

// Выполнить левое вращение поддерева, корнем которого является n:
// 0 - успешно выполненная операция
// 1 - вращение невозможно
int rotate_left(node* n, tree* t) {
	if (n->righ == NULL) {
		return 1;
	}
	else {
		node* parent, * temp;
		temp = n->righ;
		n->righ = n->righ->left;
		temp->left = n;
		parent = find_parent(t->root, NULL, n->value);
		if (parent == NULL) {
			t->root = temp;
			return 0;
		}
		else {
			if (parent->left->value == n->value) {
				parent->left = temp;
				return 0;
			}
			else {
				parent->righ = temp;
				return 0;
			}
		}
	}
}


typedef struct struct_item {
	void* payload;
	struct struct_item* next;
} item;

typedef struct struct_queue {
	item* begin;
	item* end;
}queue;

queue* create_queue() {
	queue* res = malloc(sizeof(queue));
	res->begin = NULL;
	res->end = NULL;
	return res;
}

void queue_push(queue* q, void* payload) {
	item* i = malloc(sizeof(item));
	i->next = NULL;
	i->payload = payload;

	if (q->end == NULL) {
		q->begin = i;
		q->end = i;
	}
	else {
		q->end->next = i;
		q->end = i;
	}
}

void* queue_pop(queue* q) {
	if (q->begin == NULL) return NULL;
	item* current = q->begin;
	q->begin = q->begin->next;
	if (q->begin == NULL) q->end = NULL;
	void* payload = current->payload;
	free(current);
	return payload;
}

// Вывести все значения из поддерева, корнем которого является n
// по уровням начиная с корня.
// Каждый уровень выводится на своей строке. 
// Элементы в строке разделяются пробелом. Если элемента нет, заменить на _. 
// Если дерево пусто, вывести -
void print(node* root) {
	queue* q_current = NULL;
	queue* q_next = create_queue();
	queue_push(q_next, (void*)root);

	int isNotLastLevel;
	do {
		free(q_current);
		q_current = q_next;
		q_next = create_queue();
		void* payload;

		isNotLastLevel = 0;
		while (q_current->begin != NULL) {
			payload = queue_pop(q_current);
			if (payload != NULL) {
				node* n = (node*)payload;
				printf("%d ", n->value);
				queue_push(q_next, n->left);
				queue_push(q_next, n->righ);
				isNotLastLevel = isNotLastLevel || n->left || n->righ;
			}
			else {
				printf("_ ");
				queue_push(q_next, NULL);
				queue_push(q_next, NULL);
			}
		}
		puts("");
	} while (isNotLastLevel);

	free(q_current);
	while (q_next->begin != NULL) queue_pop(q_next);
	free(q_next);
	printf("\n");
};

// Вывести все значения дерева t, аналогично функции print
void print_tree(tree* t) {
	if (t->root == NULL) {
		printf("-\n\n");
	}
	else {
		print(t->root);
	}
};


int main() {
	tree tree;
	node* node1, * node2;
	init(&tree);
	int a;

	for (int i = 0; i < 4; i++) {
		(void)scanf("%d", &a);
		insert(&tree, a);
	}
	print_tree(&tree);

	for (int i = 0; i < 3; i++) {
		(void)scanf("%d", &a);
		insert(&tree, a);
	}
	print_tree(&tree);

	(void)scanf("%d", &a);
	node1 = find(&tree, a);
	if (node1 == NULL) {
		printf("-\n\n");
	}
	else {
		node2 = find_parent(tree.root, NULL, a);
		if (node2 == NULL) {
			printf("_ ");
		}
		else {
			printf("%d ", node2->value);
		}
		if (node1->left == NULL) {
			printf("_ ");
		}
		else {
			printf("%d ", node1->left->value);
		}
		if (node1->righ == NULL) {
			printf("_ \n\n");
		}
		else {
			printf("%d \n\n", node1->righ->value);
		}
	}

	(void)scanf("%d", &a);
	node1 = find(&tree, a);
	if (node1 == NULL) {
		printf("-\n\n");
	}
	else {
		node2 = find_parent(tree.root, NULL, a);
		if (node2 == NULL) {
			printf("_ ");
		}
		else {
			printf("%d ", node2->value);
		}
		if (node1->left == NULL) {
			printf("_ ");
		}
		else {
			printf("%d ", node1->left->value);
		}
		if (node1->righ == NULL) {
			printf("_ \n\n");
		}
		else {
			printf("%d \n\n", node1->righ->value);
		}
	}

	(void)scanf("%d", &a);
	remove_node(&tree, a);
	print_tree(&tree);

	while (tree.root!=NULL && tree.root->righ != NULL) {
		rotate_left(tree.root, &tree);
	}
	print_tree(&tree);

	while (tree.root != NULL && tree.root->left != NULL) {
		rotate_right(tree.root, &tree);
	}
	print_tree(&tree);

	printf("%d\n\n", how_many_items(tree.root));

	clean(&tree);
	print_tree(&tree);
	return 0;
};
