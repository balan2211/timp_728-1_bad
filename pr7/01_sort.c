﻿#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#pragma warning(disable:4996)



void logupd(int* a, FILE* file, int size) {
    for (int i = 0; i < size; i++) {
        fprintf(file, "%d ", a[i]);
        printf("%d ", a[i]);
    }
    fprintf(file, "\n");
    printf("\n");
}

void qs(int* a, int low, int high, int n, FILE* file) {
    
    int pivot = a[(low + high) / 2], i = low, j = high;
    while (i <= j) {
        while (a[i] < pivot) {
            i++;
        }

        while (a[j] > pivot) {
            j--;
        }

        if (i <= j) {
            logupd(a, file, n);
            int temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;

        }
    }
    if (high > i) {
        qs(a, i, high, n, file);
    }

    if (low < j) {
        qs(a, low, j, n, file);
    }
}

void secf(int *a, int root, int bottom,int size, FILE *file)
{
    int maxChild; 
    bool done = false; 
    while ((root * 2 <= bottom) && (!done))
    {
        
        if (root * 2 == bottom) {
            maxChild = root * 2;
        }
        else {
            if (a[root * 2] > a[root * 2 + 1])
                maxChild = root * 2;
            else
                maxChild = root * 2 + 1;
        }
        if (a[root] < a[maxChild])
        {
            logupd(a, file, size);
            int temp = a[root]; 
            a[root] = a[maxChild];
            a[maxChild] = temp;
            root = maxChild;
        }
        else {
            done = true;
        }
    }
}

void  heapSort(int* a, int array_size,FILE *file)
{
    for (int i = (array_size / 2) - 1; i >= 0; i--) {
        //logupd(a, file, array_size);
        secf(a, i, array_size - 1, array_size, file);
    }
    for (int i = array_size - 1; i >= 1; i--)
    {
        logupd(a, file, array_size);
        int temp = a[0];
        a[0] = a[i];
        a[i] = temp;
        //logupd(a, file, array_size);
        secf(a, 0, i - 1, array_size, file);
    }
    //logupd(a, file, array_size);
}

int main()
{
    int n;
    FILE* log;
    log = fopen("quicksort.log", "w");
    scanf("%d", &n);
    int *a = malloc(sizeof(int) * n);
    int *b = malloc(sizeof(int) * n);

    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        b[i] = a[i];
    }

    qs(a, 0, n - 1, n, log);
    logupd(a, log, n);
    printf("\n");
    log = fopen("heapsort.log", "w");

    heapSort(b, n, log);
    logupd(a, log, n);

    return 0;
}